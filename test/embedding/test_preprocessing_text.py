from unittest import TestCase

from src.embeddings.preprocessing_text import preprocess_text

test_document_1 = "He is a strong guy without any deseases. But after the last surgery @hospital ¼))? pain " \
                  "" \
                  "" \
                  "resurrected.?"


class Test(TestCase):
    def test_preprocess_text(self):
        self.assertEqual(preprocess_text(test_document_1),"strong guy without deseases last surgery hospital pain resurrected")

def test_preprocess_text_wo_stopwords(self):
     self.assertEqual(preprocess_text(test_document_1, stopwords=False),
                             "he is strong guy without any deseases but after the last surgery hospital pain resurrected")

def test_preprocess_text_wo_stopwords_minlength(self):
    self.assertEqual(preprocess_text(test_document_1,stopwords=False, min_length=3),
                          "strong guy without any deseases but after the last surgery hospital pain resurrected")