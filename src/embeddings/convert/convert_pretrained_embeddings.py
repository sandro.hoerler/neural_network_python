'''
Module holds functions for addind pretrained vector models to embedding layers

sandro.hoerler@fhgr.ch

'''
import codecs

import numpy as np
from tqdm import tqdm

def word2vec_vectors_to_embedding_layer_text(file):
    # load word2vec model to embedding matrix
    # works with text file
    # first line in file is only metadata
    print('loading word embeddings...')
    embeddings_index = {}
    f = codecs.open(
        file,
        encoding='utf-8')
    i = 0
    words = []
    for line in tqdm(f):
        if i is 0:
            i += 1
            #skip first line in file, it only contains metadata
            continue
        i += 1
        values = line.rstrip().rsplit(' ')
        word = values[0]
        words.append(word)
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()
    print('found %s word vectors' % len(embeddings_index))
    return embeddings_index, words, len(embeddings_index)


def word2vec_vectors_to_embedding_layer(word_model):
    # word2vec loads pretrained vector model into own matrix
    embedding_matrix = np.zeros((len(word_model.wv.vocab) + 1, 300))
    for i, vec in enumerate(word_model.wv.vectors):
        embedding_matrix[i] = vec
    return embedding_matrix


def fasttext_vectors_to_embedding_layer(word_model):
    # puts words as dict indexes and vectors as words values
    print('loading fasttext embeddings...')
    vocab_and_vectors = {}
    for line in word_model:
        values = line.split()
        word = values[0]  # .decode('utf-8')
        vector = np.asarray(values[1:], dtype='float32')
        vocab_and_vectors[word] = vector
    print('found %s word vectors' % len(vocab_and_vectors))
    embedding_matrix = np.zeros(len(vocab_and_vectors), 100)
    words_not_found = []
    for word, i in vocab_and_vectors.items():
        if i >= len(vocab_and_vectors):
            continue
        embedding_vector = vocab_and_vectors.get(word)
        if (embedding_vector is not None) and len(embedding_vector) > 0:
            # words not found in mebedding index will be all-zeros
            embedding_matrix[i] = embedding_vector
        else:
            words_not_found.append(word)
    print('Number of null word embeddings: %d' % np.sum(np.sum(embedding_matrix, axis=1) == 0))
    return vocab_and_vectors


def fasttext_facebook_to_embedding_layer(model, dimension=100):
    vocab_size = len(model)  # TODO: check length
    print(vocab_size)
    embedding_matrix = np.random.random(vocab_size, dimension)
    model.wv


def open_gzip_file(path):
    pass
