from keras.preprocessing.text import Tokenizer
from gensim.models.fasttext import FastText
import numpy as np
import re
import matplotlib.pyplot as plt
import nltk
from string import punctuation
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer as stemmer
from nltk.tokenize import sent_tokenize
from nltk import WordPunctTokenizer
en_stop = set(nltk.corpus.stopwords.words('english'))
'''
Fasttext training logic including
* preprocessing
* lemmitation
'''
# enable or disable lemmitation

def preprocess_text(document, lematize=True, stopwords=True, min_length=1):
    # Remove all the special characters
    document = re.sub(r'\W', ' ', str(document))

    # remove all single characters
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)

    # Remove single characters from the start
    document = re.sub(r'\^[a-zA-Z]\s+', ' ', document)

    # Substituting multiple spaces with single space
    document = re.sub(r'\s+', ' ', document, flags=re.I)

    # Removing prefixed 'b'
    document = re.sub(r'^b\s+', '', document)

    # Converting to Lowercase
    document = document.lower()

    # Lemmatization
    tokens = document.split()
    if lematize:
        tokens = [stemmer.lemmatize(stemmer,word) for word in tokens]
    if stopwords:
        tokens = [word for word in tokens if word not in en_stop]
    tokens = [word for word in tokens if len(word) > min_length]

    preprocessed_text = ' '.join(tokens)

    return preprocessed_text