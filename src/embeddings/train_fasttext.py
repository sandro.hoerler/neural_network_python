'''
Created on Feb 13, 2020

@author: sandro
'''
import tempfile, shutil, os
import fasttext
from src.embeddings.preprocessing_text import preprocess_text

'''
Train fasttext model
'''

def train_fasttext_model(corpusfile, model_location, compress_model=False):
    """trains fasttext model after preprocessing """
    with open(corpusfile) as file:
        data = file.read()
    data = preprocess_text(data, min_length=3)
    temp_file = open("corpus_temp_data.tmp", "w")
    temp_file.write(data)
    temp_file.close()
    model = fasttext.train_unsupervised("corpus_temp_data.tmp", model='skipgram',minCount=5)
    model.save_model(model_location + ".bin")
    os.remove("corpus_temp_data.tmp")

def load_fasttext_model(model_location):
    return fasttext.load_model(model_location)

# if __name__ == "main":
#    train_fasttext_model("/home/sandro/data/projects/03_integrity/Korpus_EN/extracted_withdate_2020/'23 September 2011_59_19.txt'","fasttext_model")
train_fasttext_model(
    "/home/sandro/data/projects/03_integrity/Korpus_EN/extracted_withdate_2020/23 September 2011_59_19.txt",
    "fasttext_model")

