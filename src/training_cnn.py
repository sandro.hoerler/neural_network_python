from __future__ import absolute_import, division, print_function, unicode_literals

import codecs
import gzip
from os import listdir
from os import path
import re

import keras
import pandas as pd
from gensim.models import FastText, fasttext
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from keras_preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from keras import callbacks
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# import src.vocabulary as Vocabulary
from vocabulary import Vocabulary
from embeddings.convert import convert_pretrained_embeddings
from tqdm import tqdm
import embeddings.convert.convert_pretrained_embeddings as converter
from keras.preprocessing.text import Tokenizer


'''
Created on Feb 6, 2020

@author: sandro.hoerler@fhgr.ch
'''

# integrity_corpus = "/home/sandro/data/en/trainingset_latest/"
integrity_corpus = "/home/sandro/data/projects/03_integrity/Korpus_EN/trainingset_latest/"
'''
Load corpora
'''

'''
Removes stopwords and lowercases and splits
'''

def preprocess_also_remove_stopwords(text, stopwords, debug=False):
    text = re.sub(REPLACE_NO_SPACE, "", text.read().lower())
    text = re.sub(REPLACE_WITH_SPACE, " ", text)
    get_max_length(text)
    words = []
    for word in text.split(" "):
        word = word.strip()
        if word not in stopwords and word != "" and word != "&":
            words.append(word)
    if debug is True:
        print(words)
    return " ".join(words)

max_length = 0

def get_max_length(text):
    global max_length
    if max_length < len(text):
        max_length = len(text)

def load_corpus(location, folder):
    corpus = []
    for file in listdir(location + folder):
        corpus.append(
                preprocess_also_remove_stopwords(open(path.join(location + folder, file), "r", encoding="utf-8"), ""))
    return corpus;

def prepare_embedding_matrix(embeddings_index, words, word_count, embedding_dimension=300):
    # creates word_embbedding matrix out of wordembedding model
    words_not_found = []
    embedding_matrix = np.zeros((word_count, embedding_dimension))
    for i, word in enumerate(words):
        if i >= word_count:
            continue
        embedding_vector = embeddings_index.get(word)
        if (embedding_vector is not None) and len(embedding_vector) > 0:
            embedding_matrix[i] = embedding_vector
        else:
            words_not_found[i] = word
    print('number of null word embeddings: %d' % np.sum(np.sum(embedding_matrix, axis=1) == 0))
    return embedding_matrix

'''
Show file contents for debugging purpose
'''

def show_loaded_data(list):
    for i in list:
        print(i.read())

vocab_size = 5000
embedding_dim = 64
max_length = 200
trunc_type = 'post'
padding_type = 'post'
oov_tok = '<OOV>'
training_portion = .8

articles = []
labels = []
articles_test = []
articles_labels = []

REPLACE_NO_SPACE = re.compile(
        "(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])$&%=#•~@£©®→°€™›♥←×§″′Â█½à…“★”–●â►¢²¬░¶↑±¿▾¦║―¥▓▒¼⊕▼▪†■▀¨▄♫☆é♦¤▲è¾Ã∞∙↓、,│»♪╩╚³・╦╣╔╗▬❤ïØ¹≤‡√")
REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")

positive_corpus = load_corpus(integrity_corpus, "positive")
negative_corpus = load_corpus(integrity_corpus, "negative")
positive_corpus.append(load_corpus(integrity_corpus, "testset/positive"))
negative_corpus.append(load_corpus(integrity_corpus, "testset/negative"))

# positive_corpus = positive_corpus[:200]
# negative_corpus = negative_corpus[:50]

data_labeled = list(zip(positive_corpus, np.ones(len(positive_corpus))))
data_labeled.extend(list(zip(negative_corpus, np.zeros(len(negative_corpus)))))
labels = list(np.ones(len(positive_corpus)))
labels.extend(list(np.zeros(len(negative_corpus))))

tokenizer = Tokenizer(num_words = 300000)
sentences = []
for sentence, label in data_labeled:
    sentences.append(sentence)

tokenizer.fit_on_texts(sentences)
word_index = tokenizer.word_index
X = tokenizer.texts_to_sequences(sentences)
X = pad_sequences(X)

# prepare the labels
y = pd.get_dummies(labels)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)

# file
model_file = 'best_model.h5'

print(len(X_train), 'train sequences')
print(len(X_test), 'test sequences')

# model = fasttext.load_facebook_model(
#     "/home/sandro/data/projects/03_integrity/Word2VecModels/Embeddings/fasttext/fasttext_fullcorpus_german.bin")

# embeddings_index, words, word_count = converter.word2vec_vectors_to_embedding_layer_text(
#         "/home/sandro/data/english_serialied-300-vector-5-5.txt")

embeddings_index, words, word_count = converter.word2vec_vectors_to_embedding_layer_text(
        "/home/sandro/data/projects/03_integrity/Word2VecModels/Embeddings/word2vec/English/english_serialied-300-vector-5-5.txt")

embedding_matrix = prepare_embedding_matrix(embeddings_index, words, word_count, 300)

config = tf.ConfigProto(device_count={"CPU": 4})
keras.backend.tensorflow_backend.set_session(tf.Session(config=config))

print('Build model...')
model = Sequential()
# model.add(Embedding(max_features, embedding_size, input_length=max_review_length))
model.add(Embedding(word_count, 300, weights=[embedding_matrix], input_length=X.shape[1],
                    trainable=False))
model.add(Dropout(0.25))
model.add((32))
model.add(Dense(32))
model.add(Dense(2))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
print(model.summary())
print('Train...')
checkpoint = callbacks.ModelCheckpoint(model_file, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

history = model.fit(X_train, y_train,
          batch_size=64,
          epochs=100,
          validation_data=(X_test, y_test))
score, acc = model.evaluate(X_test, y_test, batch_size=1)
print(history.history.keys())

print('Test score:', score)
print('Test accuracy:', acc)
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()